<?php

/**
 * @file
 * CleanTalk module admin functions.
 */

/**
 * Cleantalk settings form.
 */
function cleantalk_settings_form($form, &$form_state) {
  $ct_comments_default = 3;

  $ct_authkey = \Drupal::config('cleantalk.settings')->get('cleantalk_authkey');
  // @FIXME
// Could not extract the default value because it is either indeterminate, or
// not scalar. You'll need to provide a default value in
// config/install/cleantalk.settings.yml and config/schema/cleantalk.schema.yml.
$ct_comments = \Drupal::config('cleantalk.settings')->get('cleantalk_comments');
  $ct_automod = \Drupal::config('cleantalk.settings')->get('cleantalk_automod');
  $ct_ccf = \Drupal::config('cleantalk.settings')->get('cleantalk_ccf');
  $ct_link = \Drupal::config('cleantalk.settings')->get('cleantalk_link');

  $form['cleantalk_authkey'] = array(
    '#type' => 'textfield',
    '#title' => t('Access key'),
    '#size' => 20,
    '#maxlength' => 20,
    '#default_value' => $ct_authkey ? $ct_authkey : '',
    '#description' => t(
      'Click <a target="_blank" href="!ct_link">here</a> to get access key.',
      array(
        '!ct_link' => \Drupal\Core\Url::fromUri('http://cleantalk.org/register?platform=drupal'),
      )
    ),
  );

  $form['cleantalk_comments'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum approved comments per registered user'),
    '#size' => 5,
    '#maxlength' => 5,
    '#default_value' => $ct_comments,
    '#element_validate' => array('element_validate_integer_positive'),
    '#description' => t('Moderate messages of guests and registered users who have approved messages less than this value (must be more than 0).'),
  );

  $form['cleantalk_automod'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable automoderation'),
    '#default_value' => $ct_automod,
    '#description' => t('Automatically publish good messages and put bad ones to admin approvement.') .
    '<br /><span class="admin-enabled">' .
    t('Note: It overrides "Skip comment approval" permissions') .
    '</span>',
  );
  
  $form['cleantalk_ccf'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable custom contact forms checking'),
    '#default_value' => $ct_ccf,
    '#description' => t('Enabling this option will aloow you to check all submissions to your site.'),
  );
  
  $form['cleantalk_link'] = array(
    '#type' => 'checkbox',
    '#title' => t('Tell others about CleanTalk'),
    '#default_value' => $ct_link,
    '#description' => t('Checking this box places a small link under the comment form that lets others know what anti-spam tool protects your site.'),
  );

  return system_settings_form($form);
}
