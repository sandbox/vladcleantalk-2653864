<?php /**
 * @file
 * Contains \Drupal\cleantalk\EventSubscriber\BootSubscriber.
 */

namespace Drupal\cleantalk\EventSubscriber;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class BootSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [KernelEvents::REQUEST => ['onEvent', 0]];
  }

  public function onEvent(\Symfony\Component\HttpKernel\Event\GetResponseEvent $event) {
    if (isset($_POST['cleantalk_authkey'])) {
      $url = 'http://moderate.cleantalk.org/api2.0';
      $dt = [
        'auth_key' => $_POST['cleantalk_authkey'],
        'method_name' => 'check_message',
        'message' => 'This message is a test to check the connection to the CleanTalk servers.',
        'example' => NULL,
        'agent' => CLEANTALK_USER_AGENT,
        'sender_ip' => $_SERVER['REMOTE_ADDR'],
        'sender_email' => 'good@cleantalk.org',
        'sender_nickname' => 'CleanTalk',
        'js_on' => 1,
      ];
      sendRawRequest($url, $dt, TRUE);
    }
    $ct_ccf = \Drupal::config('cleantalk.settings')->get('cleantalk_ccf');
    if (sizeof($_POST) > 0 && !isset($_POST['form_build_id']) && !isset($_POST['form_id']) && $ct_ccf == 1) //Custom Contact forms
 {
      $sender_email = '';
      $sender_nickname = '';
      $subject = '';
      $message = '';
      $contact_form = TRUE;
      get_fields_any($sender_email, $message, $sender_nickname, $subject, $contact_form, $_POST);

      if ($sender_email != '') {
        $ct_authkey = \Drupal::config('cleantalk.settings')->get('cleantalk_authkey');

        if (!isset($_COOKIE['ct_checkjs'])) {
          $checkjs = NULL;
        }
        elseif ($_COOKIE['ct_checkjs'] == _cleantalk_get_checkjs_value()) {
          $checkjs = 1;
        }
        else {
          $checkjs = 0;
        }

        $data = [
          'type' => 'comment',
          'auth_key' => $ct_authkey,
          'method_name' => 'check_message',
          'message' => $message,
          'example' => NULL,
          'agent' => CLEANTALK_USER_AGENT,
          'sender_ip' => $_SERVER['REMOTE_ADDR'],
          'sender_email' => $sender_email,
          'sender_nickname' => $sender_nickname,
          'message_title' => $subject,
          'message_body' => '',
          'example_title' => '',
          'example_body' => '',
          'example_comments' => '',
          'comment_body' => '',
          'js_on' => $checkjs,
        ];

        $result = _cleantalk_check_spam($data);
        if ($result['allow'] === 0) {
          drupal_set_message($result['ct_result_comment'], 'error');
        }
      }
    }
    if (isset($_POST['mergevars'])) {
      $sender_email = NULL;
      $message = '';

      foreach ($_POST['mergevars'] as $key => $value) {
        if ($sender_email === NULL && preg_match("/^\S+@\S+\.\S+$/", $value)) {
          $sender_email = $value;
        }
        else {
          $message .= "$value\n";
        }
      }

      if ($sender_email != NULL) {

        $url = 'http://moderate.cleantalk.org/api2.0';

        $ct_authkey = \Drupal::config('cleantalk.settings')->get('cleantalk_authkey');

        if (!isset($_COOKIE['ct_checkjs'])) {
          $checkjs = NULL;
        }
        elseif ($_COOKIE['ct_checkjs'] == _cleantalk_get_checkjs_value()) {
          $checkjs = 1;
        }
        else {
          $checkjs = 0;
        }

        $data = [
          'type' => 'comment',
          'auth_key' => $ct_authkey,
          'method_name' => 'check_message',
          'message' => $message,
          'example' => NULL,
          'agent' => CLEANTALK_USER_AGENT,
          'sender_ip' => $_SERVER['REMOTE_ADDR'],
          'sender_email' => $sender_email,
          'sender_nickname' => '',
          'message_title' => '',
          'message_body' => '',
          'example_title' => '',
          'example_body' => '',
          'example_comments' => '',
          'comment_body' => '',
          'js_on' => $checkjs,
        ];

        $result = _cleantalk_check_spam($data);
        if ($result['allow'] === 0) {
          drupal_set_message($result['ct_result_comment'], 'error');
          $_POST['mergevars']['EMAIL'] = '';
        }
      }
    }
  }

}
