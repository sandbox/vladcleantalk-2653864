<?php

/**
 * @file
 * Contains \Drupal\cleantalk\Form\CleantalkSettingsForm.
 */

namespace Drupal\cleantalk\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

class CleantalkSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cleantalk_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('cleantalk.settings');

    foreach (Element::children($form) as $variable) {
      $config->set($variable, $form_state->getValue($form[$variable]['#parents']));
    }
    $config->save();

    if (method_exists($this, '_submitForm')) {
      $this->_submitForm($form, $form_state);
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['cleantalk.settings'];
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $ct_comments_default = 3;

    $ct_authkey = \Drupal::config('cleantalk.settings')->get('cleantalk_authkey');
    // @FIXME
    // Could not extract the default value because it is either indeterminate, or
    // not scalar. You'll need to provide a default value in
    // config/install/cleantalk.settings.yml and config/schema/cleantalk.schema.yml.
    $ct_comments = \Drupal::config('cleantalk.settings')->get('cleantalk_comments');
    $ct_automod = \Drupal::config('cleantalk.settings')->get('cleantalk_automod');
    $ct_ccf = \Drupal::config('cleantalk.settings')->get('cleantalk_ccf');
    $ct_link = \Drupal::config('cleantalk.settings')->get('cleantalk_link');

    $form['cleantalk_authkey'] = [
      '#type' => 'textfield',
      '#title' => t('Access key'),
      '#size' => 20,
      '#maxlength' => 20,
      '#default_value' => $ct_authkey ? $ct_authkey : '',
      '#description' => t('Click <a target="_blank" href="http://cleantalk.org/register?platform=drupal">here</a> to get access key.'),
    ];

    $form['cleantalk_comments'] = [
      '#type' => 'textfield',
      '#title' => t('Minimum approved comments per registered user'),
      '#size' => 5,
      '#maxlength' => 5,
      '#default_value' => $ct_comments,
      /*'#element_validate' => [
        'element_validate_integer_positive'
        ],*/
      '#description' => t('Moderate messages of guests and registered users who have approved messages less than this value (must be more than 0).'),
    ];

    $form['cleantalk_automod'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable automoderation'),
      '#default_value' => $ct_automod,
      '#description' => t('Automatically publish good messages and put bad ones to admin approvement.') . '<br /><span class="admin-enabled">' . t('Note: It overrides "Skip comment approval" permissions') . '</span>',
    ];

    $form['cleantalk_ccf'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable custom contact forms checking'),
      '#default_value' => $ct_ccf,
      '#description' => t('Enabling this option will aloow you to check all submissions to your site.'),
    ];

    $form['cleantalk_link'] = [
      '#type' => 'checkbox',
      '#title' => t('Tell others about CleanTalk'),
      '#default_value' => $ct_link,
      '#description' => t('Checking this box places a small link under the comment form that lets others know what anti-spam tool protects your site.'),
    ];

    return parent::buildForm($form, $form_state);
  }

}
